# While loop #
 While loops are used to execute a set of statements repeatedly until a particular condition is satisfied.

 https://beginnersbook.com/2015/03/while-loop-in-java-with-examples/
 
# Do-while loop #
do-while loop is similar to while loop, however there is a difference between them: In while loop, condition is evaluated before the execution of loop’s body but in do-while loop condition is evaluated after the execution of loop’s body.
 
 https://beginnersbook.com/2015/03/do-while-loop-in-java-with-example/